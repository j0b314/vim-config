execute pathogen#infect()

" Do not be compatible with vi
set nocompatible

" Remap leader key
let mapleader = ","

" Autodetect filetypes
filetype plugin on

" Nice colorscheme
colorscheme blue

" Activate syntax highlighting
syntax on

" Set window to bigger size
set lines=50
set columns=145

" Set gui font
set guifont=Consolas:h11

" Disable toolbar
set guioptions-=T
set guioptions-=m

" Map menubar and make it toggle via F9
nnoremap <F9> :if &go=~#'m'<Bar>set go-=m<Bar>else<Bar>set go+=m<Bar>endif<CR>

" Show line numbers
set number

" Move on visual lines, not file lines
nnoremap k gk
nnoremap gk k
nnoremap j gj
nnoremap gj j

" Set tabstop to 4 spaces (use no tabs, show and use it as 4 spaces)
" Rhapsody default
set tabstop=4
set shiftwidth=4
set expandtab
set smarttab

" Enable colorcolumn
set colorcolumn=80
 
" Perform auto-indent
set autoindent
set smartindent

" Relative line numbering
set relativenumber

" Show tabs as |
"set list
set listchars=tab:>-

" Activate/Deactivate visible tabs
noremap <F2> :set list!<CR>
inoremap <F2> <C-o>:set list!<CR>
cnoremap <F2> <C-o>:set list!<CR>

" Set wildmenu autocompletion
set wildmenu
set wildmode=longest:full,full

" Show ruler
set ruler

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Always show the status line
set laststatus=2

" Format status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\ \ Column:\ %c

" Returns true if paste mode is enabled 
function! HasPaste() 
    if &paste 
        return 'PASTE MODE  ' 
    endif 
    return '' 
endfunction

" Disable arrow keys (use ya homebar! ;) )
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

inoremap <Up> <Nop>
inoremap <Down> <Nop>
inoremap <Left> <Nop>
inoremap <Right> <Nop>

" Escape from insert mode via jj
inoremap jj <ESC>

" Map <leader>w to create vertical split window and move into it
nnoremap <leader>w <C-w>v<C-w>l

" Move between windows with CTRL-hjkl
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Open terminal below
nnoremap <leader>t :bo term<CR>

" Open NERDTree
map <F12> :NERDTreeToggle <CR>

" Enable Smarter tabline from airline
let g:airline#extensions#tabline#enabled = 1
" Does not work under windows!
" let g:airline_powerline_fonts = 1 

" Map switch between buffers to F6 and F7, clear Window with F5 (bufkill)
noremap <F5> :BD<CR>
noremap <F6> :bp<CR>
noremap <F7> :bn<CR>

" Open CtrlPTag
nnoremap <C-l> :CtrlPTag<CR>

" Shortcut F11 for scratchpad
noremap <F11> :e H:/Privat/scratchpad.txt<CR>

" Configure Tagbar
map <F8> :TagbarToggle<CR>
"let g:tagbar_ctags_bin = "C:/ctags58/ctags.exe"

" Enable PECL regex (Perl compatible regular expressions) in search
nnoremap / /\v
vnoremap / /\v

" Seach intelligent, lowercase will be case insensitive. One uppercase
" transforms to sensitive
set ignorecase
set smartcase

" Substitute all occurences in a line
set gdefault

" Highlight your search results
set incsearch
set showmatch
set hlsearch

" Bind a key do clear search highlighting
nnoremap <leader><space> :noh<CR>

